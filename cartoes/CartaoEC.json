{
  "swagger": "2.0",
  "info": {
    "title": "Gestão de Cartões de Estabelecimentos Comerciais",
    "description": "API responsável por gerenciar as informações dos cartões de compras dos estabelecimentos comerciais.",
    "version": "1.0.0"
  },
  "host": "api-devportal.vr.com.br",
  "basePath": "/gestao-cartao-ec/v1",
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json",
    "application/json; charset=utf-8"
  ],
  "parameters": {
    "ClientId": {
      "name": "client_id",
      "in": "header",
      "description": "Client ID disponibilizado na criação da App.",
      "required": true,
      "type": "string"
    },
    "AccessToken": {
      "name": "access_token",
      "in": "header",
      "description": "Access Token com permissões de acesso.",
      "required": true,
      "type": "string"
    },
    "Cnpj": {
      "name": "cnpj",
      "in": "path",
      "description": "Número do CNPJ do portador/beneficiário do cartão.",
      "required": true,
      "pattern": "\\d{14}",
      "type": "string"
    },
    "quantidadeDeDiasParaExtrato": {
      "name": "quantidadeDeDiasParaExtrato",
      "in": "query",
      "description": "Numero de dias para o filtro",
      "required": true,
      "pattern": "\\d{2}",
      "type": "number"
    },
    "pagina": {
      "name": "pagina",
      "in": "query",
      "description": "Numero a pagina",
      "required": true,
      "pattern": "\\d",
      "type": "number"
    }
  },
  "externalDocs": {
    "description": "Detalhes da API",
    "url": "https://dev.vr.com.br/api-portal/node/54#section6"
  },
  "paths": {
    "/estabelecimentos-comerciais/{cnpj}/saldo": {
      "get": {
        "tags": [
          "Cartão"
        ],
        "description": "Obter saldo de um cartão compras de um determinado estabelecimento comercial.",
        "parameters": [
          {
            "$ref": "#/parameters/ClientId"
          },
          {
            "$ref": "#/parameters/AccessToken"
          },
          {
            "$ref": "#/parameters/Cnpj"
          }
        ],
        "responses": {
          "200": {
            "description": "Saldo do cartão compras do Estabelecimento Comercial retornado com sucesso.",
            "schema": {
              "$ref": "#/definitions/Saldo"
            }
          },
          "400": {
            "description": "Bad Request."
          },
          "401": {
            "$ref": "#/responses/Unauthorized"
          },
          "403": {
            "description": "Forbidden."
          },
          "404": {
            "description": "Not Found."
          },
          "415": {
            "description": "Unsupported Media Type."
          },
          "429": {
            "description": "Too Many Requests."
          },
          "500": {
            "$ref": "#/responses/InternalServerError"
          },
          "502": {
            "$ref": "#/responses/BadGateway"
          },
          "504": {
            "$ref": "#/responses/GatewayTimeout"
          }
        }
      }
    },
    "/estabelecimentos-comerciais/{cnpj}/extrato": {
      "get": {
        "tags": [
          "Cartão"
        ],
        "description": "Obter extrato de um cartão compras de um determinado estabelecimento comercial.",
        "parameters": [
          {
            "$ref": "#/parameters/ClientId"
          },
          {
            "$ref": "#/parameters/AccessToken"
          },
          {
            "$ref": "#/parameters/Cnpj"
          },
          {
            "$ref": "#/parameters/quantidadeDeDiasParaExtrato"
          },
          {
            "$ref": "#/parameters/pagina"
          }
        ],
        "responses": {
          "200": {
            "description": "Extrato do cartão compras do Estabelecimento Comercial retornado com sucesso.",
            "schema": {
              "$ref": "#/definitions/Extrato"
            }
          },
          "400": {
            "description": "Bad Request."
          },
          "401": {
            "$ref": "#/responses/Unauthorized"
          },
          "403": {
            "description": "Forbidden."
          },
          "404": {
            "description": "Not Found."
          },
          "415": {
            "description": "Unsupported Media Type."
          },
          "429": {
            "description": "Too Many Requests."
          },
          "500": {
            "$ref": "#/responses/InternalServerError"
          },
          "502": {
            "$ref": "#/responses/BadGateway"
          },
          "504": {
            "$ref": "#/responses/GatewayTimeout"
          }
        }
      }
    }
  },
  "responses": {
    "Unauthorized": {
      "description": "Unauthorized.",
      "schema": {
        "$ref": "#/definitions/Unauthorized"
      }
    },
    "InternalServerError": {
      "description": "Internal Server Error.",
      "schema": {
        "$ref": "#/definitions/InternalError"
      }
    },
    "BadGateway": {
      "description": "Bad Gateway.",
      "schema": {
        "type": "string",
        "description": "Mensagens, Avisos, Erros Padrão."
      }
    },
    "GatewayTimeout": {
      "description": "Gateway Timeout.",
      "schema": {
        "type": "string",
        "description": "Mensagens, Avisos, Erros Padrão."
      }
    }
  },
  "definitions": {
    "Extrato": {
      "description": "Entidade representando informações de extrato do cartão compras do estabelecimento comercial.",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "valor": {
            "description": "Valor da transação",
            "type": "string",
            "format": "string",
            "pattern": "^\\d+(\\.\\d{1,2})?$",
            "example": "125.12"
          },
          "descricao": {
            "description": "Descrição da transação",
            "type": "string",
            "maxLength": 20,
            "minLength": 1,
            "example": "Consumo - IRI MARKET"
          },
          "data_hora": {
            "description": "Data e Hora da transação",
            "type": "string",
            "pattern": "^((((19|[2-9]\\d)\\d{2})-(0[13578]|1[02])-(0[1-9]|[12]\\d|3[01]))|(((19|[2-9]\\d)\\d{2})-(0[13456789]|1[012])-(0[1-9]|[12]\\d|30))|(((19|[2-9]\\d)\\d{2})-02-(0[1-9]|1\\d|2[0-8]))|(((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-02-29))([Tt])([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]([Zz])$",
            "example": "2018-02-28T09:50:12:000Z",
            "format": "date-time"
          }
        }
      }
    },
    "Saldo": {
      "description": "Entidade representando informações relacionadas retorno da consulta saldo cartao compras.",
      "type": "object",
      "properties": {
        "codigo_produto": {
          "description": "Código do produto",
          "type": "string",
          "maxLength": 5,
          "minLength": 5,
          "pattern": "\\d{5}",
          "example": "00027"
        },
        "saldo": {
          "description": "Saldo do cartão",
          "type": "string",
          "format": "float",
          "pattern": "^\\d+(\\.\\d{1,2})?$",
          "example": 132412425.12
        }
      }
    },
    "Unauthorized": {
      "description": "Entidade repsentando informações de erro de autorização.",
      "type": "object",
      "required": [
        "mensagem"
      ],
      "properties": {
        "mensagem": {
          "description": "Mensagem de erro.",
          "type": "string"
        }
      }
    },
    "InternalError": {
      "description": "Entidade representando informações do erro ocorrido no backend.",
      "type": "object",
      "required": [
        "mensagem"
      ],
      "properties": {
        "codigo": {
          "description": "Codigo de erro",
          "type": "integer"
        },
        "mensagem": {
          "description": "Mensagem de erro.",
          "type": "string"
        }
      }
    }
  }
}